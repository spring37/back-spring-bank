package com.globalKnowledge.gestibank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globalKnowledge.gestibank.models.Admin;
import com.globalKnowledge.gestibank.models.Agent;
import com.globalKnowledge.gestibank.models.Client;
import com.globalKnowledge.gestibank.models.Compte;
import com.globalKnowledge.gestibank.models.Notification;
import com.globalKnowledge.gestibank.models.Utilisateur;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>{

}
