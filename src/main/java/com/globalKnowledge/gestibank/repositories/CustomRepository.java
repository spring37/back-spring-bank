package com.globalKnowledge.gestibank.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import com.globalKnowledge.gestibank.models.AuthDTO;
import com.globalKnowledge.gestibank.models.Utilisateur;



@Repository
public class CustomRepository {

	@Autowired
	private JdbcTemplate template;
	
	public Utilisateur authentification (AuthDTO auth) {
		String email=auth.getEmail();
		String mdp=auth.getMdp();
		String sql="SELECT * from utilisateur WHERE email="+ "\""+ email + "\""+ " AND mdp=" +"\"" +mdp + "\";";
		List<Utilisateur> result= new ArrayList<Utilisateur>();
		result = this.template.query(sql, new BeanPropertyRowMapper<Utilisateur>(Utilisateur.class));
		System.out.println((Utilisateur)result.get(0));
		if (!result.isEmpty()) {
			return (Utilisateur) result.get(0);
		}else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Erreur e-mail ou password ");
		}
	}
}
