package com.globalKnowledge.gestibank.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.globalKnowledge.gestibank.models.Compte;

@Repository
public interface CompteRepository extends JpaRepository<Compte, Long>{
    @Query("FROM Compte WHERE client_idclient = ?1")
    List<Compte> findByIdClient(Long client_idclient);

}
