package com.globalKnowledge.gestibank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globalKnowledge.gestibank.models.Admin;
import com.globalKnowledge.gestibank.models.Agent;
import com.globalKnowledge.gestibank.models.Utilisateur;

@Repository
public interface AgentRepository extends JpaRepository<Agent, Long>{

}
