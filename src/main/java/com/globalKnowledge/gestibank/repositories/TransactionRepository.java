package com.globalKnowledge.gestibank.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.globalKnowledge.gestibank.models.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{
    @Query("FROM Transaction WHERE compte_idcompte = ?1")
    List<Transaction> findByIdCompte(Long client_idclient);
}
