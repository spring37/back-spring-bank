package com.globalKnowledge.gestibank.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.globalKnowledge.gestibank.models.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{
    @Query("FROM Client WHERE utilisateur_idutilisateur = ?1")
    Optional<Client> findByIdUtilisateur(Long utilisateur_idutilisateur);

}
