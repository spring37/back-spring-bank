package com.globalKnowledge.gestibank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalKnowledge.gestibank.models.Admin;
import com.globalKnowledge.gestibank.models.Agent;
import com.globalKnowledge.gestibank.models.Utilisateur;
import com.globalKnowledge.gestibank.repositories.AdminRepository;
import com.globalKnowledge.gestibank.repositories.AgentRepository;
import com.globalKnowledge.gestibank.repositories.UtilisateurRepository;

@Service
public class AgentService {

	@Autowired
	private AgentRepository repository;
		
	public List<Agent> findAll() {
		return this.repository.findAll();
	}

	public Optional<Agent> findById(Long id) {
		return this.repository.findById(id);
	}

}
