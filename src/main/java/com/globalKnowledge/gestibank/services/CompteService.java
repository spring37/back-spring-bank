package com.globalKnowledge.gestibank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalKnowledge.gestibank.models.Admin;
import com.globalKnowledge.gestibank.models.Agent;
import com.globalKnowledge.gestibank.models.Client;
import com.globalKnowledge.gestibank.models.Compte;
import com.globalKnowledge.gestibank.models.Utilisateur;
import com.globalKnowledge.gestibank.repositories.AdminRepository;
import com.globalKnowledge.gestibank.repositories.AgentRepository;
import com.globalKnowledge.gestibank.repositories.ClientRepository;
import com.globalKnowledge.gestibank.repositories.CompteRepository;
import com.globalKnowledge.gestibank.repositories.UtilisateurRepository;

@Service
public class CompteService {

	@Autowired
	private CompteRepository repository;
		
	public List<Compte> findAll() {
		return this.repository.findAll();
	}

	public Optional<Compte> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Compte> findByIdClient(Long id) {
		return this.repository.findByIdClient(id);
	}

}
