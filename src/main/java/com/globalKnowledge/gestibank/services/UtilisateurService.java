package com.globalKnowledge.gestibank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalKnowledge.gestibank.models.AuthDTO;
import com.globalKnowledge.gestibank.models.Utilisateur;
import com.globalKnowledge.gestibank.repositories.CustomRepository;
import com.globalKnowledge.gestibank.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository repository;
	@Autowired
	private CustomRepository custrepository;
	
	// Pour faire la v�rif
	public Utilisateur authentification(AuthDTO auth) {
		return this.custrepository.authentification(auth);
	}
	
	public Utilisateur save(Utilisateur entity) {
		return this.repository.save(entity);
	}
	
	public Utilisateur saveOrUpdate(Utilisateur entity) {
		return this.repository.save(entity);
	}
	
	
	public List<Utilisateur> findAll() {
		return this.repository.findAll();
	}

	public Optional<Utilisateur> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}
