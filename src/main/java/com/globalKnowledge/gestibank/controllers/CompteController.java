package com.globalKnowledge.gestibank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globalKnowledge.gestibank.models.Compte;
import com.globalKnowledge.gestibank.services.CompteService;


@RestController
@RequestMapping("comptes")

@CrossOrigin
public class CompteController {
	
	@Autowired
	private CompteService service;

	@GetMapping("")
	public List<Compte> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("{id}")
	public Optional<Compte> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("client/{id}")
	public List<Compte> findByIdClient(@PathVariable Long id){
		return this.service.findByIdClient(id);
	}
}
