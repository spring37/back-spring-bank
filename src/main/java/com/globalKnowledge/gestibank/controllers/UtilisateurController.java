package com.globalKnowledge.gestibank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globalKnowledge.gestibank.models.AuthDTO;
import com.globalKnowledge.gestibank.models.Utilisateur;
import com.globalKnowledge.gestibank.services.UtilisateurService;


@RestController
@RequestMapping("utilisateurs")

@CrossOrigin
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService service;

	
	// Pour faire de la verif
	@PostMapping("login")
	public Utilisateur authentification(@RequestBody AuthDTO entity ){
		return this.service.authentification(entity);
	}
	
	@GetMapping("")
	public List<Utilisateur> findAll(){
		return this.service.findAll();
	}
	
	
	@PostMapping("")
	public Utilisateur save(@RequestBody Utilisateur entity){
		return this.service.save(entity);
	}
	
	
	@PutMapping("")
	public Utilisateur saveOrUpdate(@RequestBody Utilisateur entity){
		return this.service.saveOrUpdate(entity);
	}
	

	@GetMapping("{id}")
	public Optional<Utilisateur> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	
	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
		
}
