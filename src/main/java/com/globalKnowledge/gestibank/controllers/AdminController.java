package com.globalKnowledge.gestibank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globalKnowledge.gestibank.models.Admin;
import com.globalKnowledge.gestibank.models.Utilisateur;
import com.globalKnowledge.gestibank.services.AdminService;
import com.globalKnowledge.gestibank.services.UtilisateurService;


@RestController
@RequestMapping("admins")

@CrossOrigin
public class AdminController {
	
	@Autowired
	private AdminService service;

	@GetMapping("")
	public List<Admin> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("{id}")
	public Optional<Admin> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
}
