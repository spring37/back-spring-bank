package com.globalKnowledge.gestibank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globalKnowledge.gestibank.models.Admin;
import com.globalKnowledge.gestibank.models.Agent;
import com.globalKnowledge.gestibank.models.Client;
import com.globalKnowledge.gestibank.models.ClientPotentiel;
import com.globalKnowledge.gestibank.models.Utilisateur;
import com.globalKnowledge.gestibank.services.AdminService;
import com.globalKnowledge.gestibank.services.AgentService;
import com.globalKnowledge.gestibank.services.ClientPotentielService;
import com.globalKnowledge.gestibank.services.ClientService;
import com.globalKnowledge.gestibank.services.UtilisateurService;


@RestController
@RequestMapping("clientsPotentiels")

@CrossOrigin
public class ClientPotentielController {
	
	@Autowired
	private ClientPotentielService service;

	@GetMapping("")
	public List<ClientPotentiel> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("{id}")
	public Optional<ClientPotentiel> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
}
