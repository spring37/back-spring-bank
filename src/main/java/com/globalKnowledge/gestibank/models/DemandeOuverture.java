package com.globalKnowledge.gestibank.models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "demandeouverture")
@NoArgsConstructor
@AllArgsConstructor
public class DemandeOuverture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long iddemandeOuverture;
    @Column()
	private Date dateDemande;
    @Column()
	private int valide;//boolean pose probleme ?
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "clientPotentiel_idclientPotentiel", nullable = true)
    private ClientPotentiel clientPotentiel;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "admin_idadmin", nullable = true)
    private Admin admin;
	    
}

