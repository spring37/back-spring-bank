package com.globalKnowledge.gestibank.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "compte")
@NoArgsConstructor
@AllArgsConstructor
public class Compte {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idcompte;
    @Column()
	private String numCompte;
    @Column()
	private String rib;
    @Column()
	private Double solde;
    @Column()
	private Double decouvert;
    @Column()
	private Double montantAgios;
    @Column()
	private Double seuilRemuneration;
    @Column()
	private Double montantRemuneration;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_idclient", nullable = true)
    private Client client;
		    
}

