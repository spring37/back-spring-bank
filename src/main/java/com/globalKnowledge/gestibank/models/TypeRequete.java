package com.globalKnowledge.gestibank.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "typeRequete")
@NoArgsConstructor
@AllArgsConstructor
public class TypeRequete {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idtypeRequete;
    @Column()
	private String type;
	    
}

