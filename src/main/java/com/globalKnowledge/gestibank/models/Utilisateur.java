package com.globalKnowledge.gestibank.models;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "utilisateur")
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idutilisateur;
    @Column()
	private String nom;
	@Column()
	private String prenom;
	@Column()
	@Email
	private String email;
	@Column()
	private String adresse;
	@Column()
	private String telephone;
	@Column()
	private String pseudo;
	@Column()
	private String mdp;

	
	//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    //@ManyToOne(fetch = FetchType.LAZY, optional = false)
    //@JoinColumn(name = "projet_idprojet", nullable = true)
    //private Projet projet;
	    
}

