package com.globalKnowledge.gestibank.models;

import lombok.Data;

@Data
public class AuthDTO {
	private String email;
	private String mdp;
}
